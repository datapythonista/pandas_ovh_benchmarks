#!/bin/sh

# set up server
wget https://github.com/conda-forge/miniforge/releases/latest/download/Mambaforge-Linux-x86_64.sh
bash Mambaforge-Linux-x86_64.sh -b
./mambaforge/bin/mamba init
source .bashrc
git clone https://github.com/pandas-dev/pandas.git
cd pandas
git checkout 2a2daf787c5bf4015729f6660d189389d3d7dc47
mamba env create
mamba activate pandas-dev
./setup.py build_ext --inplace -j2
cd asv_bench
asv machine --yes

# repeat benchmarks for same commit
for i in $(seq 1 1000); do
    asv run --python=same --set-commit-hash=2a2daf787c5bf4015729f6660d189389d3d7dc47
    mv results/$(hostname)/2a2daf78-existing-py_home_ubuntu_mambaforge_envs_pandas-dev_bin_python.json results/$(hostname)/run${i}_2a2daf78-existing-py_home_ubuntu_mambaforge_envs_pandas-dev_bin_python.json
done
