# pandas OVH benchmarks

This repo contains the code and results for the test for running the pandas benchmark
suite in OVH infrastructure.

## Tests

Two main tests have been performed:

- `same_commit`: Running the benchmarks for the same commit around 18 times in each of
   3 different instances
- `incremental`: Running the benchmarks for one day of commits, where all or almost all
   commits should not affect the benchmarks much

The goal is to identify the variance in the runs. Ideally, all benchmarks would take
exactly the same. In practice, at least small differences will exist. But to track
pandas code performance over time, the differences should be very small (e.g. +/- 1%).
If differences are big, tracking benchmarks over time would not help detect performance
regressions, as it'd be impossible to discriminate actual regressions from noise.

## Results

For `same_commit`, the main results are saved in the file `same_commit_results.parquet`
and analyzed in the `Analyze_results_same_commit` Jupyter notebook.

For `incremental`, the main results are the asv rendered website in `incremental/html`.

Raw results are in the json files in the corresponding directories.
